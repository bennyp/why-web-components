import resolve from 'rollup-plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

const opts = {
  keep_classnames: true,
  keep_fnames: true,
  mangle: false,
};


export default [{
  input: 'index.js',
  plugins: [resolve(), terser({...opts, module: true })],
  output: {
    file: 'modern.js',
    format: 'es',
  }
}, {
  input: 'index.js',
  plugins: [resolve(), terser({...opts, ecma: 5 })],
  output: {
    file: 'legacy.js',
    format: 'iife',
    name: 'WhyWebComponents',
  }
}];
